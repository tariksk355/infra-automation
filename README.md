# AWS Infrastructure Deployment with Terraform and DevSecOps Principles

This project automates the deployment of AWS infrastructure using Terraform, while integrating security best practices through DevSecOps. It provisions EC2 instances, IAM roles, security groups, and networking resources on AWS.

## Key Features

**Infrastructure as Code (IaC):** Automated AWS resource provisioning with Terraform.
Security Integration: Utilizes TfSec to scan for security vulnerabilities in Terraform configurations.

**CI/CD Automation:** GitLab CI/CD pipelines automate the entire process, from initialization to deployment and security scanning.

**IAM Role and Policy Management:** Secure IAM roles for EC2 instances with necessary AWS policies.

**Networking Configuration:** Automated VPC, subnet, and security group setup for a robust AWS network environment.

## DevSecOps Practices

This project follows DevSecOps principles by embedding security checks into the CI/CD pipeline. TfSec is used to scan the infrastructure for potential security risks and misconfigurations before deployment, ensuring compliance and security.

SSH access to EC2 instances is restricted, with port 22 blocked to enhance security. Instead of SSH, AWS Systems Manager (SSM) is used for secure instance management and access, providing a more secure method for administration.

## How to Use

Configure AWS credentials in your GitLab CI/CD environment.
Push changes to the repository to trigger the pipeline.
The infrastructure will be automatically deployed and scanned for security issues.
Monitor the pipeline results and review the security scan report.

## Conclusion

This project demonstrates how to deploy secure, scalable AWS infrastructure using Terraform, integrated with continuous security checks via DevSecOps practices.